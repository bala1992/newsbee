import React from 'react'
import { render } from 'react-dom'
import axios from 'axios'
import '../public/normalize.css'
import '../public/style.css'
import NewsTile from './NewsTile'

const App = React.createClass({
    getInitialState () {
        return {
            news: {}
        }
    },
    componentDidMount () {
        axios.get(`https://newsapi.org/v1/articles?source=techcrunch&apiKey=6a875481460744449d45d5f60b6a9937`)
        .then((response) => {
            this.setState({news: response.data})
        })
        .catch((error) => console.error('axios error', error))
    },
    render () {
        var tiles = []
        var tileObj = {}
        if(this.state.news.articles){            
            for(let i=0; i<this.state.news.articles.length; i++){               
                tileObj.urlToImage = this.state.news.articles[i].urlToImage
                tileObj.title = this.state.news.articles[i].title
                tileObj.description = this.state.news.articles[i].description
                tileObj.author = this.state.news.articles[i].author
                tiles.push(JSON.parse(JSON.stringify(tileObj)))
            }
            console.log(tiles)
        }else{
            tiles = []
        }
        return (
            <div className="App">
                <h2>NewsBee</h2>
                {tiles.map((news) => {
                    return (
                        <NewsTile key={news.title} {...news} />
                    )
                })}           
            </div>
        )
    }
})

render(<App />, document.getElementById('app'))