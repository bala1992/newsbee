import React from 'react'
const { string } = React.PropTypes

const NewsTile = React.createClass({
    propTypes: {
        urlToImage: string.isRequired,
        author: string.isRequired,
        description: string.isRequired,
        title: string.isRequired
    },
    render () {
        const { urlToImage, author, description, title } = this.props
        return (
           <div className="newsTile">
                <div className="imageHolder">
                    <img src={urlToImage} alt={title} />
                </div>
                <div className="descHolder">
                    <h4>{title}</h4>
                    <p>{description}</p>
                    <p>{author}</p>
                </div>
                <div className="clearfix"></div>
            </div>
        )
    }
})

export default NewsTile